Group: group 140

Question
========

RQ: Is there any difference in the mean Aggregate Rating among different Price Range in Zomato API Analysis?

Null hypothesis: There is no difference in the mean Aggregate Rating among different Price Range in Zomato API Analysis.

Alternative hypothesis: There is difference in the mean Aggregate Rating among different Price Range in Zomato API Analysis.

Dataset
=======

URL: https://www.kaggle.com/shrutimehta/zomato-restaurants-data?select=zomato.csv

Column Headings:

```
 [1] "Restaurant.ID"        "Restaurant.Name"     
 [3] "Country.Code"         "City"                
 [5] "Address"              "Locality"            
 [7] "Locality.Verbose"     "Longitude"           
 [9] "Latitude"             "Cuisines"            
[11] "Average.Cost.for.two" "Currency"            
[13] "Has.Table.booking"    "Has.Online.delivery" 
[15] "Is.delivering.now"    "Switch.to.order.menu"
[17] "Price.range"          "Aggregate.rating"    
[19] "Rating.color"         "Rating.text"         
[21] "Votes"
```